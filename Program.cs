﻿string filePath = "./assets/professional-life.tsv";

bool isCounted(string item1, string item2)
{
    return item1 == item2;
}

double getPercentage(string data, string[] array)
{
    int totalData = array.Length;
    string _ref = data.ToLower();
    int count = 0;

    foreach (var item in array)
    {
        if (isCounted(item.ToLower(), _ref))
        {
            count++;
        }
    }

    return count * 100 / totalData;
};

double getAbsoluteFrequency(string data, string[] array)
{
    string _ref = data.ToLower();
    int count = 0;

    foreach (var item in array)
    {
        if (isCounted(item.ToLower(), _ref))
        {
            count++;
        }
    }

    return count;
};

double getRelativeFrequency(string data, string[] array)
{
    int totalData = array.Length;
    string _ref = data.ToLower();
    int count = 0;

    foreach (var item in array)
    {
        if (isCounted(item.ToLower(), _ref))
        {
            count++;
        }
    }

    return (double)count / totalData;
};

void generateTableData(string elem, string[] array, List<string> set, List<string[]> table)
{
    string modifiedElem = elem.ToLower().Trim();
    if (!set.Contains(modifiedElem))
    {
        set.Add(modifiedElem);
        string[] row = new string[]{
      elem != "" ? elem : "no answer",
      getAbsoluteFrequency(elem ?? "", array).ToString(),
      getRelativeFrequency(elem ?? "", array).ToString("N2"),
      getPercentage(elem ?? "", array).ToString("N2") + " %"
    };
        table.Add(row);
    }
};

void printTable(List<string[]> table)
{
    // row
    foreach (var item in table)
    {
        // column
        foreach (var item2 in item)
        {
            Console.Write(item2.PadRight(35));
        }
        Console.WriteLine();
    }
}

if (File.Exists(filePath))
{
    using (FileStream fstream = File.OpenRead(filePath))
    {
        using (StreamReader reader = new(fstream))
        {
            string[] header = new string[]{
        "Absolute Frequency",
        "Relative Frequency",
        "Percentage"
      };

            string? line;
            List<string[]> csv = new();

            while ((line = reader.ReadLine()) != null)
            {
                csv.Add(line.Split('\t'));
            }

            List<string[]> csvNoTitle = csv.GetRange(1, csv.Count - 1);

            int columnIndex1 = 23;
            int columnIndex2 = 5;
            int columnIndex3 = 14;

            string titleTable1 = csv[0][columnIndex1];
            string titleTable2 = csv[0][columnIndex2];
            string titleTable3 = csv[0][columnIndex3];

            string[] column1 = csvNoTitle.Select(elem => elem[columnIndex1]).ToArray();
            string[] column2 = csvNoTitle.Select(elem => elem[columnIndex2]).ToArray();
            string[] column3 = csvNoTitle.Select(elem => elem[columnIndex3]).ToArray();

            List<string> set1 = new();
            List<string> set2 = new();
            List<string> set3 = new();

            List<string[]> table1 = new()
      { new string[] {
          titleTable1,
          header[0],
          header[1],
          header[2]
      }};
            List<string[]> table2 = new()
      { new string[] {
          titleTable2,
          header[0],
          header[1],
          header[2]
      }};
            List<string[]> table3 = new()
      { new string[] {
          titleTable3,
          header[0],
          header[1],
          header[2]
      }};

            // table generation
            foreach (var item in column1)
            {
                generateTableData(item, column1, set1, table1);
            }
            foreach (var item in column2)
            {
                generateTableData(item, column2, set2, table2);
            }
            foreach (var item in column3)
            {
                generateTableData(item, column3, set3, table3);
            }

            /**
             *  Print Table
             */
            Console.WriteLine("Table 1 ---------------------------------\n");
            printTable(table1);
            Console.WriteLine("\n\nTable 2 ---------------------------------\n");
            printTable(table2);
            Console.WriteLine("\n\nTable 3 ---------------------------------\n");
            printTable(table3);
        }
    }
}
